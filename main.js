
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmZmJkOGMyMC00MGUyLTQzZDMtYjZkOC1iMjYwYTAyZDkzZjMiLCJpZCI6OTkwNiwic2NvcGVzIjpbImFzciIsImdjIl0sImlhdCI6MTU1NTI1MTAxNn0.u_YYy-QCMFEhTbLKT5diVqUbofcKAkbvzy0zfVK_B-g';

var viewer = new Cesium.Viewer('cesiumContainer', {
    shouldAnimate : true,
    shadows : true,
});
viewer.clock.shouldAnimate = true;



action_1();

//
// viewer = new Cesium.Viewer('cesiumContainer', {
//     shouldAnimate : true,
//     shadows : true,
// });
// viewer.clock.shouldAnimate = true;
// viewer.scene.terrainProvider = new Cesium.createWorldTerrain();
// var layers = viewer.scene.imageryLayers;
// var basicBasemap = layers.addImageryProvider(new Cesium.createWorldImagery({
//     style : Cesium.IonWorldImageryStyle.AERIAL_WITH_LABELS
// }));
// action_2();

function action_1(){

    var layers = viewer.scene.imageryLayers;

    // var blackMarble = layers.addImageryProvider(new Cesium.IonImageryProvider({ assetId: 3812 }));
    // blackMarble.alpha = 0.3;
    // blackMarble.brightness = 1.0;
    //
    // var basicBasemap = layers.addImageryProvider(new Cesium.createWorldImagery({
    //     style : Cesium.IonWorldImageryStyle.AERIAL_WITH_LABELS
    // }));
    setBillboardProperties();
    flyToRectangle(-5.0,26.0, 34.0, 55.0);

    var startPosition = new Cesium.Cartesian3.fromDegrees(28.97835, 41.00823);
    var endPosition = new Cesium.Cartesian3.fromDegrees(31.23571, 30.04441);
    drawLine(startPosition, endPosition);
}

function action_2(){

    addZurichBuilding();
    addFigures();
    // flyToRectangle(8.5014,47.3544, 8.5963, 47.3905);
    // flyToRectangle(8.54,47.369, 8.542, 47.371);
}


function flyToRectangle(west, south, east, north) {
    var rectangle = Cesium.Rectangle.fromDegrees(west, south, east, north);
    viewer.camera.flyTo({
        destination : rectangle
    });
}

function drawLine(startPosition, endPosition){

    var entity = {
        id: 10,
        polyline: {
            positions: new Cesium.CallbackProperty(function() {
                return [startPosition, startPosition];
            }, true),
            width: 5,
            material: Cesium.Color.RED
        }
    };

//wait some time until Cesium is loaded
//as there is no ready event yet (https://github.com/AnalyticalGraphicsInc/cesium/issues/4422)
    window.setTimeout(function() {
        var startTime = viewer.clock.currentTime;
        var endTime = Cesium.JulianDate.addSeconds(startTime, 3, new Cesium.JulianDate());

        var property = new Cesium.SampledProperty(Cesium.Cartesian3);
        property.addSample(startTime, startPosition);
        property.addSample(endTime, endPosition);

        function interpolatePosition(time) {
            var currentLine;

            if (Cesium.JulianDate.compare(time, endTime) < 0) {
                //interpolation going on
                var currentPosition = property.getValue(time);
                currentLine = [startPosition, currentPosition];
            } else {
                currentLine = [startPosition, endPosition];

                //make property constant again to save performance
                //however this causes the entity to blink (http://stackoverflow.com/questions/36747621/updating-cesium-callback-property-causes-the-entity-to-flash)
                entity.polyline.positions.setCallback(function() {
                    return currentLine;
                }, true);
            }

            return currentLine;
        }

        viewer.entities.add(entity);
        //make property dynamic
        entity.polyline.positions.setCallback(interpolatePosition, false);
    }, 15000);
}

function setBillboardProperties() {
    viewer.entities.add({
        //zurich
        id: 1,
        position : Cesium.Cartesian3.fromDegrees(8.3470, 47.4468),
        billboard : {
            show : true,
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },

    });

    viewer.entities.add({
        //Cairo
        id: 2,
        position : Cesium.Cartesian3.fromDegrees(31.23571, 30.04441),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.15, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },
    });


    viewer.entities.add({
        //Luxor
        id: 3,
        position : Cesium.Cartesian3.fromDegrees(32.2679, 25.8867),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },
    });

    viewer.entities.add({
        //Kasimli
        id: 4,
        position : Cesium.Cartesian3.fromDegrees(31.36929, 41.12611),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        }
    });

    viewer.entities.add({
        //Madrid
        id: 5,
        position : Cesium.Cartesian3.fromDegrees(-3.70379, 40.41677),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },
    });


    viewer.entities.add({
        //Istanbul
        id: 6,
        position : Cesium.Cartesian3.fromDegrees(28.97835, 41.00823),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.15, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },
    });


    viewer.entities.add({
        //Marlloca
        id: 7,
        position : Cesium.Cartesian3.fromDegrees(3.11023, 39.9038),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },
    });

    viewer.entities.add({
        //Parma
        id: 8,
        position : Cesium.Cartesian3.fromDegrees(2.86857, 39.48392),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        },
    });

    viewer.entities.add({
        // London
        id: 9,
        position : Cesium.Cartesian3.fromDegrees(-0.12521, 51.49274),
        billboard : {
            image : './resource/Points.png', // default: undefined
            scale : 0.07, // default: 1.0
            verticalOrigin : Cesium.VerticalOrigin.BOTTOM, // default: CENTER
        }
    });


}



function addZurichBuilding() {

// add the resource file
    var tileset = new Cesium.Cesium3DTileset({
        url: './resource/tileset.json'
    });
    viewer.scene.primitives.add(tileset);

//Highlight The Hotel building
    tileset.readyPromise.then(function(tileset) {
        var properties = tileset.properties;
        if (Cesium.defined(properties) && Cesium.defined(properties.UUID)) {
            tileset.style = new Cesium.Cesium3DTileStyle({
                color: {
                    conditions: [
                        ["${UUID} === '{85737873-8208-4533-B670-B3920D860ADA}'", "color('cyan')"],
                        ["true", "rgba(${red}, ${green}, ${blue}, 1)"]
                    ]
                }
            });
        }
    });

}


function addFigures() {

    var lat = 47.3693;
    var lon = 8.54188;

    var modelPosition = Cesium.Cartesian3.fromDegrees(lon, lat, 455);

//make model look to the south
    var hpr = new Cesium.HeadingPitchRoll(Cesium.Math.toRadians(-90), 0, 0);
    var orientation = Cesium.Transforms.headingPitchRollQuaternion(modelPosition, hpr);

    var entity = viewer.entities.add({
        name : "Pine",
        position : modelPosition,
        orientation : orientation,
        model : {
            uri : './resource/AngelaBurr.glb',
            minimumPixelSize: 20.0,
            heightReference:Cesium.HeightReference.CLAMP_TO_GROUND,
        }
    });
    viewer.zoomTo(entity);
}
